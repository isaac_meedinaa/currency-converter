package example.isaac.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void convertClick(View view) {

        EditText amountEditText = (EditText) findViewById(R.id.et_one);
        int a = Integer.parseInt(amountEditText.getText().toString());
        double euroValue = a*0.85;

        Toast.makeText(this, "$" + amountEditText.getText().toString() + " is " + euroValue + " €", Toast.LENGTH_SHORT).show();
    }
}
